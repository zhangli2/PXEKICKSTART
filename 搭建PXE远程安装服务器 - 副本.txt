PXE远程装机的好处：

*规模化：同时装配多台服务器

*自动化：安装系统、配置各种服务

*远程实现：不需要光盘、U盘等安装介质
1）准备Centos7安装源
mkdir -p /var/ftp/centos7
mkdir /media/cdrom/
mount /dev/sr0 /media/cdrom/
cd /media/cdrom/Packages/
rpm -ivh vsftpd-3.0.2-22.el7.x86_64.rpm 
umount /dev/sr0
vim /etc/fstab
/dev/cdrom /var/ftp/centos7 iso9660 defaults 0 0
mount -a
systemctl start vsftpd
systemctl enable vsftpd
curl ftp://192.168.137.10  (本机IP）
2）安装并启用TFTP服务
yum install -y tftp-server xinetd 
vi /etc/xinetd.d/tftp

disable                 = no

systemctl start xinetd
systemctl enable xinetd
3)准备LINUX内核，初始化镜像文件
cd /media/cdrom/images/pxeboot/
cp vmlinuz initrd.img /var/lib/tftpboot/
4）准备PXE引导程序、启动菜单文件
yum -y install syslinux
cp /usr/share/syslinux/pxelinux.0 /var/lib/tftpboot/
mkdir /var/lib/tftpboot/pxelinux.cfg
cp /media/cdrom/isolinux/isolinux.cfg /var/lib/tftpboot/pxelinux.cfg/default 
vi /var/lib/tftpboot/pxelinux.cfg/default

default auto  //指定默认入口名称
prompt 1   //1表示等待用户控制，0表示不等待用户控制
label auto   //图像按照
  kernel vmlinuz
  append initrd=initrd.img repo=ftp://192.168.9.101/centos7  ks=ftp://192.168.9.101/ks.cfg

systemctl restart xinetd
5)安装并启用DHCP服务
yum install -y dhcp
cp /usr/share/doc/dhcp-4.2.5/dhcpd.conf.example  /etc/dhcp/dhcpd.conf
vi /etc/dhcp/dhcpd.conf 

subnet 192.168.137.0 netmask 255.255.255.0 {
        option routers 192.168.137.10;
        range 192.168.137.150 192.168.137.200;
        next-server 192.168.137.10;   //PXE服务器IP地址
        filename "pxelinux.0";  //PXE引导程序的文件名
}

systemctl start dhcpd
systemctl enable dhcpd

