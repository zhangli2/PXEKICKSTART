问题描述
使用yum源安装完system-config-kickstart软件包，system-config-kickstart命令生成ks文件文件时，出现以下问题：软件包无法下载。
问题解决
修改yum源配置文件，将yum源配置文件中的id部分修改位development后，卸载system-config-kickstart并重新安装该命令，即可出现软件包信息，示例如下：
[root@node1 kk]# vim /etc/yum.repos.d/my.repo 

[centos]    --此处修改为[development]
name=my-centos7
baseurl=file:///mnt
enabled=1
gpgcheck=0

#只需将文件内[centos]修改位[development]即可，如下
[root@node1 kk]# cat /etc/yum.repos.d/my.repo 
[development]
name=my-centos7
baseurl=file:///mnt
enabled=1
gpgcheck=0